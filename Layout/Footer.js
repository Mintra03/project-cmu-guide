import React from 'react'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'
import { goBack } from 'connected-react-router'
import { view, Text } from 'react-native'
import {
    FooterContainer,
    HeaderText,
    HeaderIcon,
    Body,
    Center,
    HeaderContainer,
} from '../Components/styled-component'
import { Icon, TabBar } from '@ant-design/react-native'
import router from "../src/system/Router"

class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'redTab',
        };
    }

    renderContent(pageText) {
        return (
            <View style={{ flex: 1, alignItems: 'center', background: 'white' }}>
                <Text style={{ margin: 50 }}>{pageText}</Text>
            </View>
        );
    }
    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }

    render() {
        const { selectedTab = 'blueTab', goToHomePage, goToMapPage, goToListPage, goToProfilePage} = this.props
        const Tab = selectedTab

        return (
            <TabBar
                unselectedTintColor="#949494"
                tintColor="#742688"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="HOME"
                    icon={<Icon name="home" />}
                    selected={Tab === 'home'}
                    onPress={goToHomePage}
                />

                <TabBar.Item
                    title="MAP"
                    icon={<Icon name="environment" />}
                    selected={Tab === 'map'}
                    onPress={goToMapPage}
                />

                <TabBar.Item
                    title="LIST"
                    icon={<Icon name="unordered-list" />}
                    selected={Tab === 'list'}
                    onPress={goToListPage}
                />

                <TabBar.Item
                    title="PROFILE"
                    icon={<Icon name="user" />}
                    selected={Tab === 'profile'}
                    onPress={goToProfilePage}
                />
            </TabBar>
        )
    }
}

export default connect(
    null,
    dispatch => ({
        goToHomePage: state => dispatch(push('./HomePage')),
        goToMapPage: state => dispatch(push('./MapPage')),
        goToListPage: state => dispatch(push('./ListPage')),
        goToProfilePage: state => dispatch(push('./ProfilePage'))
    })
)
(Footer)