/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Router from './src/system/Router'
import { routerActions } from 'connected-react-router';

AppRegistry.registerComponent(appName, () => Router);
