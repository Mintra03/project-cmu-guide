import React, { Component } from 'react'
import { Route, Switch, Redirect, NativeRouter } from 'react-router-native'
import { Provider } from 'react-redux';
import FirstPage from '../pages/FirstPage'
import Login from '../pages/LoginPage'
import Register from '../pages/RegisterPage'
import Home from '../pages/HomePage'
import MapPage from '../pages/MapPage'
import List from '../pages/ListPage'
import Profile from '../pages/ProfilePage'
import EditProfile from '../pages/EditProfilePage'
import ChangPassword from '../pages/ChangPassword'
import Review from '../pages/ReviewPage'
import Shopinfor from '../pages/ShopInformationPage'

import { store, history } from '../reducers/AppStore'
import { ConnectedRouter } from 'connected-react-router'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/firstpage" component={FirstPage} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/home" component={Home} />
                        <Route exact path="/map" component={MapPage} />
                        <Route exact path="/list" component={List} />
                        <Route exact path="/profile" component={Profile} />
                        <Route exact path="/editProfile" component={EditProfile} />
                        <Route exact path="/changPassword" component={ChangPassword} />
                        <Route exact path="/review" component={Review} />
                        <Route exact path="/shopinfor" component={Shopinfor} />
                        <Redirect to="/home" />
                    </Switch>
                </ConnectedRouter>
                </Provider>
                )
            }
        }

export default Router
