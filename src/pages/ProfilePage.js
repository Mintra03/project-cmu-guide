import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, TouchableOpacity, Alert,  } from 'react-native';
import { Icon, Button, InputItem, List, TabBar, Card, SwipeAction } from '@ant-design/react-native'
import { connect } from 'react-redux'
import Container from "../../Layout/Container"

const Item = List.Item;

class ProfilePage extends Component {

    state = {
        firstName: '',
        lastName: '',
        Password: '',
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'yellowTab',
        };
    }

    goToLoginPage = () => {
        this.props.history.push('/login')
    }

    goToHomePage = () => {
        Alert.alert('Go to HomePage!!')
        this.props.history.push('/home')
    }

    goToMapPage = () => {
        Alert.alert('Go to MapPage!!')
        this.props.history.push('/map')
    }

    goToListPage = () => {
        Alert.alert('Go to ListPage!!')
        this.props.history.push('/list')
    }

    goToProfilePage = () => {
        Alert.alert('Go to ProfilePage!!')
        this.props.history.push('/profile')
    }

    render() {
        const right = [
            {
                text: 'Edit',
                onPress: () => console.log('edit'),
                style: { backgroundColor: 'orange', color: 'white' },
            }
        ];

        return (

            <TabBar
                unselectedTintColor="#949494"
                tintColor="#742688"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="HOME"
                    icon={<Icon name="home" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToHomePage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="MAP"
                    icon={<Icon name="environment" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToMapPage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="LIST"
                    icon={<Icon name="unordered-list" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToListPage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="Profile"
                    icon={<Icon name="user" />}
                    selected={this.state.selectedTab === 'yellowTab'}
                    onPress={this.goToProfilePage}
                >
                    <View style={styles.container2}>
                        <ImageBackground source={require('./119.jpeg')} style={{ height: 200 }}>
                            <View style={styles.container}>
                                <View style={styles.header}>
                                    <Image style={styles.avatar} source={require('./119.jpeg')} />
                                </View>

                                <Image style={styles.avatar} source={require('./120.jpg')} />
                                <TouchableOpacity onPress={this.goToRevenuePage} >
                                    <Icon name="camera" size="md" color="#8A848D" style={styles.logo} />
                                </TouchableOpacity>

                                <View style={styles.body}>
                                    <View style={styles.bodyContent}>
                                        <Text style={styles.name}> Min Mintra </Text>
                                        <Text style={styles.info}> mintra_panyana@cmu.ac.th </Text>

                                        <View style={[styles.box1, styles.center]}>
                                            <Card >
                                                <SwipeAction
                                                    autoClose
                                                    style={{ backgroundColor: 'transparent' }}
                                                    right={right}
                                                    onOpen={() => console.log('open')}
                                                    onClose={() => console.log('close')}
                                                >
                                                    <InputItem
                                                        clear
                                                        value={this.state.value1}
                                                        onChange={value => {
                                                            this.setState({
                                                                value1: value,
                                                            });
                                                        }}
                                                        style={styles.text2}
                                                        placeholder="Firstname"
                                                    />
                                                </SwipeAction>
                                            </Card>
                                        </View>

                                        <View style={[styles.box1, styles.center]}>
                                            <Card >
                                                <SwipeAction
                                                    autoClose
                                                    style={{ backgroundColor: 'transparent' }}
                                                    right={right}
                                                    onOpen={() => console.log('open')}
                                                    onClose={() => console.log('close')}
                                                >
                                                    <InputItem
                                                        clear
                                                        value={this.state.value1}
                                                        onChange={value => {
                                                            this.setState({
                                                                value1: value,
                                                            });
                                                        }}
                                                        style={styles.text2}
                                                        placeholder="Lastname"
                                                    />
                                                </SwipeAction>
                                            </Card>
                                        </View>

                                        <View style={[styles.box1, styles.center]}>
                                            <Card >
                                                <SwipeAction
                                                    autoClose
                                                    style={{ backgroundColor: 'transparent' }}
                                                    right={right}
                                                    onOpen={() => console.log('open')}
                                                    onClose={() => console.log('close')}
                                                >
                                                    <InputItem
                                                        clear
                                                        value={this.state.value1}
                                                        onChange={value => {
                                                            this.setState({
                                                                value1: value,
                                                            });
                                                        }}
                                                        style={styles.text2}
                                                        placeholder="Password"
                                                    />
                                                </SwipeAction>
                                            </Card>
                                        </View>

                                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }}
                                            style={[styles.backgroundColor, styles.botton]} onPress={this.goToLoginPage}>
                                            Log out
                                </Button>
                                    </View>
                                </View>

                            </View>
                        </ImageBackground >
                    </View>
                </TabBar.Item>
            </TabBar>


        );
    }
}
export default ProfilePage

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    container2: {
        flex: 1,
        backgroundColor: '#EFE5F6',
    },

    header: {
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 3,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 130
    },
    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
    },
    body: {
        flex: 1,
        marginBottom: -415,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
        marginTop: 14
    },
    name: {
        fontSize: 28,
        color: "#696969",
        fontWeight: "600"
    },
    info: {
        fontSize: 14,
        color: "#00BFFF",
        marginTop: 6,
        marginTop: 6,
    },
    description: {
        fontSize: 16,
        color: "#696969",
        marginTop: 10,
        textAlign: 'center'
    },
    buttonContainer: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
    box1: {
        backgroundColor: 'white',
        flexDirection: 'column',
        width: 240,
        height: 34,
        marginTop: 16,
    },
    logo4: {
        width: 45,
        height: 45,
        borderRadius: 11,
        marginTop: 2,
        marginLeft: 6,
    },
    text2: {
        color: '#535252',
        fontSize: 14,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginTop: 6,
        marginLeft: 15,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 160,
        height: 45,
        flexDirection: 'row',
    },
    backgroundColor: {
        backgroundColor: '#8613A5',
        borderColor: '#8613A5',
    },
    botton: {
        marginTop: 28,
    },
    footer: {
        // backgroundColor: '#A5F1CD',
        // alignItems: 'center',
        // flexDirection: 'row',
        // margin: 0.1,
        // top: 265,
        flex: 1,
    },
    logo: {
        top: 39,
        marginLeft: 210,
    },

});
