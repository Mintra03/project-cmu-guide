import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, TouchableOpacity, Alert, ScrollView, Linking } from 'react-native';
import { SearchBar, Icon, Button, InputItem, List, TabBar, Card, SwipeAction, Avatar, Grid, Carousel, WhiteSpace } from '@ant-design/react-native'
import axios from "axios";
import Container from "../../Layout/Container"

class HomePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'yellowTab',
        };
    }

    goToHomePage = () => {
        Alert.alert('Go to HomePage!!')
        this.props.history.push('/home')
    }

    goToMapPage = () => {
        Alert.alert('Go to MapPage!!')
        this.props.history.push('/map')
    }

    goToListPage = () => {
        Alert.alert('Go to ListPage!!')
        this.props.history.push('/list')
    }

    goToProfilePage = () => {
        Alert.alert('Go to ProfilePage!!')
        this.props.history.push('/profile')
    }

    render() {
        return (
            <TabBar
                unselectedTintColor="#949494"
                tintColor="#742688"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="HOME"
                    icon={<Icon name="home" />}
                    selected={this.state.selectedTab === 'yellowTab'}
                    onPress={this.goToHomePage}
                >
                    <View style={styles.container}>
                        <View >
                            <SearchBar
                                value={this.state.value}
                                placeholder="Search"
                                placeholderTextColor="#ABABAB"
                                onCancel={this.clear}
                                onChange={this.onChange}
                                cancelText="Cancle"
                            />
                        </View>

                        <View style={{ height: '36%', width: '100%', }}>
                            <ImageBackground source={require('./135.png')} style={styles.background} >
                            </ImageBackground>
                        </View>

                        

                    </View>


                </TabBar.Item>

                <TabBar.Item
                    title="MAP"
                    icon={<Icon name="environment" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToMapPage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="LIST"
                    icon={<Icon name="unordered-list" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToListPage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="Profile"
                    icon={<Icon name="user" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToProfilePage}
                >
                </TabBar.Item>
            </TabBar>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EFE5F6',
    },

    header: {
        backgroundColor: '#CBA2EC',
        // flex: 0.1,
        flexDirection: 'row',
        backgroundColor: '#CBA2EC',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 6,
        height: 55,
    },
    // box1: {
    //     flex: 0.2,
    //     margin: 1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     backgroundColor: '#CBA2EC'
    // },

    textHead: {
        textAlign: 'center',
        fontSize: 16,
        color: '#5F2789',
        marginTop: 1
    },

    textHead2: {
        textAlign: 'center',
        fontSize: 16,
        color: '#5F2789',
        marginTop: 6
    },

    textHead3: {
        fontSize: 14.5,
        color: '#5F2789',
        marginBottom: 18
    },

    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // justifyContent: 'flex-start',

    },
    content: {
        flex: 1,
    },

    background: {
        width: '100%',
        height: '100%'
    },

    row: {
        backgroundColor: '#D6D0DB',
        flex: 0.5,
        flexDirection: 'row'
    },

    box1: {
        backgroundColor: '#F6F3F9',
        flex: 1,
        margin: 1,
    },

    box2: {
        backgroundColor: '#F6F3F9',
        flex: 1,
        margin: 1,
    },

    box3: {
        backgroundColor: '#F6F3F9',
        flex: 1,
        margin: 1,
    },

    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },



});

export default HomePage