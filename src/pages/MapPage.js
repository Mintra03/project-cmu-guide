import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Icon, Button, InputItem, List, TabBar, Card, SwipeAction, Avatar } from '@ant-design/react-native'
import MapView, { AnimatedRegion, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import axios from 'axios'
import Container from "../../Layout/Container"

class MapPage extends Component {

    state = {
        selectedTab: 'yellowTab',
        shopList: [],
        current: {
            latitude: 0,
            longitude: 0,
            error: null
        }
    };

    componentDidMount() {
        this.getMap()
        navigator.geolocation.getCurrentPosition(position => {
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                error: null
            })
        },
            error => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 20000 }
        );
    }

    getMap = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR2ZQgBHpgRsWCeNlVN4QBF7NIKENrSfcz_SSJZgWbZuKjltez5KHLgVqVQ')
            .then(response => {
                console.log(response)
                this.setState({

                    shopList: response.data.data,
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getLocation = () => {
        const maps = this.state.shopList
        let location = []
        if (maps.length !== undefined) {
            console.log('length', maps.length);
            for (let i = 0; i < maps.length; i++) {
                console.log('latitude', Number(maps[i].location.latitude));
                console.log('longitude', Number(maps[i].location.longitude));
                location.push(
                    <Marker
                        identifier="Marker1"
                        coordinate={{
                            latitude: Number(maps[i].location.latitude),
                            longitude: Number(maps[i].location.longitude),
                        }}
                    />
                    // {Image = "123.png"}
                )
            }
            return location
        }
    }

    goToHomePage = () => {
        Alert.alert('Go to HomePage!!')
        this.props.history.push('/home')
    }

    goToMapPage = () => {
        Alert.alert('Go to MapPage!!')
        this.props.history.push('/map')
    }

    goToListPage = () => {
        Alert.alert('Go to ListPage!!')
        this.props.history.push('/list')
    }

    goToProfilePage = () => {
        Alert.alert('Go to ProfilePage!!')
        this.props.history.push('/profile')
    }

    render() {
        console.log('state', this.state.shopList);

        return (

            <TabBar
                unselectedTintColor="#949494"
                tintColor="#742688"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="HOME"
                    icon={<Icon name="home" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToHomePage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="MAP"
                    icon={<Icon name="environment" />}
                    selected={this.state.selectedTab === 'yellowTab'}
                    onPress={this.goToMapPage}
                >

                    <View style={styles.container2}>
                        <View style={{ flex: 1 }}>
                            <MapView
                                showsUserLocation={true}
                                showsMyLocationButton={true}
                                showsCompass={true}
                                toolbarEnabled={true}
                                zoomEnabled={true}
                                rotateEnabled={true}
                                provider={PROVIDER_GOOGLE}
                                style={styles.map}
                                region={{
                                    latitude: 18.8043949,
                                    longitude: 98.952629,
                                    latitudeDelta: 0.02,
                                    longitudeDelta: 0.0121,
                                }}
                            >
                                {this.getLocation()}
                                <Marker coordinate={this.state.current} />
                            </MapView>
                        </View>
                    </View>

                </TabBar.Item>

                <TabBar.Item
                    title="LIST"
                    icon={<Icon name="unordered-list" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToListPage}
                >
                </TabBar.Item>

                <TabBar.Item
                        title="Profile"
                        icon={<Icon name="user" />}
                        selected={this.state.selectedTab === 'blueTab'}
                        onPress={this.goToProfilePage}
                    >
                    </TabBar.Item>
                </TabBar>

                );
            }
        }
        export default MapPage
        
const styles = StyleSheet.create({
                    container: {
                    ...StyleSheet.absoluteFillObject,
                height: 400,
                width: 400,
                justifyContent: 'flex-end',
                alignItems: 'center',
            },
    map: {
                    ...StyleSheet.absoluteFillObject,
                },
            
    container2: {
                    flex: 1,
                backgroundColor: '#EFE5F6',
            },
        
    header: {
                    backgroundColor: '#CBA2EC',
                flex: 0.1,
                flexDirection: 'row',
                backgroundColor: '#CBA2EC',
                borderBottomWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 6,
                height: 55,
            },
    box1: {
                    flex: 0.5,
                margin: 8,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#CBA2EC'
            },
        
    textHead: {
                    // textAlign: 'center',
                    fontSize: 20,
                color: '#2A0D41',
            },
});