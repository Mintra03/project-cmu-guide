import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import { Form, Item, Input, Label, Button } from 'native-base';
import { WhiteSpace } from '@ant-design/react-native';
import axios from "axios";
import { Fonts } from '../../src/assets/Fonts'

class Firstpage extends React.Component {
    state = {
        username: '',
        password: '',
        email: '',
        firstname: '',
        lastname: ''
    };
    goToResgister = () => {
        this.props.history.push('/register', {
        })
    }

    goToHome = () => {
        this.props.history.push('/home', {

        })
    }

    render() {

        return (
            <ImageBackground source={require('./bg2.png')} style={styles.Background}>
                <ScrollView>
                    <View style={[styles.container]}>
                        <View style={[styles.content]}>
                            <View style={[styles.logoLayout]}>
                                <Image source={require('./logo.png')} style={[styles.logo, styles.center]}></Image>
                            </View>

                            <View style={[styles.ContentLayout]}>
                                <Form>
                                    <Item floatingLabel>
                                        <Label style={[styles.loginFont]}>Username</Label>
                                        <Input />
                                    </Item>
                                    <Item floatingLabel last>
                                        <Label style={[styles.loginFont]}>Password</Label>
                                        <Input />
                                    </Item>
                                </Form>
                            </View>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>

                            <View style={[styles.footerLayout]}>
                                <Button style={[styles.loginButton, styles.center]} onPress={this.goToHome}>
                                    <Text style={[styles.loginFont, styles.textCenter]}>Login</Text>
                                </Button>

                                <WhiteSpace></WhiteSpace>

                                <TouchableOpacity onPress={this.goToResgister}>
                                    <Text style={[styles.loginFont]}>Sign up for CMU Guide</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({

    container: {
        flex: 1

    },

    content: {
        flex: 1
    },

    Background: {
        width: '100%',
        height: '100%',
    },

    logoLayout: {
        flex: 0.4,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },

    contentLayout: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },

    footerLayout: {
        flex: 0.4,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },

    loginFont: {
        fontSize: 17,
        fontFamily: Fonts.OpenSansCondensedBold
    },

    logo: {
        width: 200,
        height: 200
    },

    loginButton: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        flex: 0.3,
        margin: 20,
        borderRadius: 5,
        width: 320,
        height: 50
    },

    textCenter: {
        textAlign: 'center'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    }


})

export default Firstpage
