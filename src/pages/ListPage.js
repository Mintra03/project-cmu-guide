import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, TouchableOpacity, Alert, ScrollView, FlatList } from 'react-native';
import { Icon, Button, InputItem, List, TabBar, Card, SwipeAction, Avatar, Grid } from '@ant-design/react-native'
import axios from "axios";
import Container from "../../Layout/Container"
import { Fonts } from '../../src/assets/Fonts'

class ListPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'yellowTab',
            items: []
        };
    }

    goToHomePage = () => {
        Alert.alert('Go to HomePage!!')
        this.props.history.push('/home')
    }

    goToMapPage = () => {
        Alert.alert('Go to MapPage!!')
        this.props.history.push('/map')
    }

    goToListPage = () => {
        Alert.alert('Go to ListPage!!')
        this.props.history.push('/list')
    }

    goToProfilePage = () => {
        Alert.alert('Go to ProfilePage!!')
        this.props.history.push('/profile')
    }

    // goBack = () => {
    //     const { goBack } = this.props
    //     console.log("go go ",this.props.transaction);
    //     const data = ()=>{
    //         return this.props.transaction.filter(data => data.transaction.walletName == this.state.walletName );
    //     }
    //     console.log("data",this.props.transaction);
    //     goBack()
    // }


    componentDidMount() {
        this.getList()
    }

    getList = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR2ZQgBHpgRsWCeNlVN4QBF7NIKENrSfcz_SSJZgWbZuKjltez5KHLgVqVQ')
            .then(response => {
                console.log(response)
                this.setState({

                    items: response.data.data,
                    isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    render() {
        return (

            <TabBar
                unselectedTintColor="#949494"
                tintColor="#742688"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="HOME"
                    icon={<Icon name="home" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToHomePage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="MAP"
                    icon={<Icon name="environment" />}
                    selected={this.state.selectedTab === 'blueTab'}
                    onPress={this.goToMapPage}
                >
                </TabBar.Item>

                <TabBar.Item
                    title="LIST"
                    icon={<Icon name="unordered-list" />}
                    selected={this.state.selectedTab === 'yellowTab'}
                    onPress={this.goToListPage}
                >

                    <View style={styles.container}>
                        <View style={styles.header}>
                            <TouchableOpacity
                                onPress={this.onClickBack}
                            >
                                <Icon name="left" size="md" color="#2A0D41" marginTop="12" />
                            </TouchableOpacity>

                            <View style={styles.icon}>
                                <Text style={styles.textHead}>Recommend</Text>
                            </View>
                        </View>

                        <ScrollView>
                            {!this.state.isLoading ? (
                                <View style={{ margin: 8 }}>
                                    <ScrollView style={{ flex: 1, backgroundColor: '#EFE5F6' }}>
                                        <FlatList
                                            style={{ flex: 1 }}
                                            inverted data={this.state.items}
                                            renderItem={({ item, index }) =>
                                                // console.log(item,'sss')

                                                (
                                                    <Card style={{ backgroundColor: 'white', margin: 5 }}>
                                                        <Card.Header
                                                            title={item.lang.th.name}
                                                            style={styles.textShopName2}
                                                            thumbStyle={{ width: 30, height: 60 }}
                                                        // extra="this is extra"
                                                        />
                                                        <Card.Body style={{ flexDirection: 'row' }}>
                                                            <View style={{ flexDirection: 'row', margin: 4 }}>
                                                                <Image source={{ uri: item.image }} style={{ width: 150, height: 150, margin: 6 }} />
                                                            </View>
                                                            <View style={{ width: 150 }}>
                                                                <Text style={styles.textShopName} >{item.lang.th.name}</Text>
                                                                <Text numberOfLines={4} style={styles.textShopName1} style={[styles.center]}>{item.lang.th.description}</Text>

                                                            </View>
                                                        </Card.Body>
                                                    </Card>
                                                )
                                            }
                                        />
                                    </ScrollView>
                                </View>
                            ) : (
                                    <ActivityIndicator size="large" color="#0000ff" style={[styles.center]} />
                                )}
                            <View style={{ padding: 30 }}>
                                <View style={[styles.rowHeader]}>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                    </TabBar.Item>

                    <TabBar.Item
                        title="Profile"
                        icon={<Icon name="user" />}
                        selected={this.state.selectedTab === 'blueTab'}
                        onPress={this.goToProfilePage}
                    >
                    </TabBar.Item>
                </TabBar>
        );
    }
}
export default ListPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EFE5F6',
    },

    header: {
        backgroundColor: '#CBA2EC',
        // flex: 0.1,
        flexDirection: 'row',
        backgroundColor: '#CBA2EC',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 6,
        height: 55,
    },
    box1: {
        flex: 0.5,
        margin: 8,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#CBA2EC'
    },

    textHead: {
        // textAlign: 'center',
        fontSize: 18,
        color: '#2A0D41',
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // justifyContent: 'flex-start',

    },

    textShopName: {
        textAlign: 'left',
        fontSize: 16,
        color: 'black',
        marginTop: 30,
    },

    textShopName1: {
        textAlign: 'left',
        fontSize: 14,
        color: '#877F8D',
    },

    textShopName2: {
        textAlign: 'left',
        fontSize: 20,
        color: '#877F8D',
    },

    rowHeader: {
        flex: 1,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },


});