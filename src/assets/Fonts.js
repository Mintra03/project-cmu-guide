export const Fonts = {
    RobotoThinItalic:'Roboto-ThinItalic',
    OpenSansCondensedBold: 'OpenSansCondensed-Bold',
    OpenSansCondensedLight: 'OpenSansCondensed-Light',
    OpenSansCondensedLightItalic: 'OpenSansCondensed-LightItalic',
    PTSansNarrowWebBold: 'PT_Sans-Narrow-Web-Bold',
    PTSansNarrowWebRegular: 'PT_Sans-Narrow-Web-Regular',
    ArimoBold: 'Arimo-Bold',
    ArimoBoldItalic: 'Arimo-BoldItalic',
    ArimoItalic: 'Arimo-Italic',
    ArimoRegular: 'Arimo-Regular',
    AbelRegular: 'Abel-Regular',
    LobsterRegular:'Lobster-Regular',
    YanoneBold:'YanoneKaffeesatz-Bold',
    ArchivoNarrowBold:'ArchivoNarrow-Bold',
    OswaldBold:'Oswald-Bold',
    AntonRegular:'Anton-Regular'
}
