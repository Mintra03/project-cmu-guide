function UserReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_USER':
            return action.user
        case 'EDIT_USER':
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}

function UsersReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_USER':
            return [...state, UserReducer(null, action)]
        case 'EDIT_USER':
            return state.map((each, index) => {
                if (each.email === action.email) {
                    return UserReducer(each, index)
                }
                return each
            })
        default:
            return state
    }
}

export default UsersReducer
